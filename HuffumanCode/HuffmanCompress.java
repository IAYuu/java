package test;

import java.math.BigDecimal;
import java.util.*;

public class HuffmanCompress {
	final int a;
	final int r;
	int k;
	static List<Elem> hcs;
	
	public HuffmanCompress() {
		// TODO Auto-generated constructor stub
		hcs = new ArrayList<Elem>();

		hcs.add(new Elem(0.18));
		hcs.add(new Elem(0.16));
		hcs.add(new Elem(0.14));
		hcs.add(new Elem(0.10));
		hcs.add(new Elem(0.10));
		hcs.add(new Elem(0.09));
		hcs.add(new Elem(0.07));
		hcs.add(new Elem(0.04));
		hcs.add(new Elem(0.04));
		hcs.add(new Elem(0.02));
		hcs.add(new Elem(0.02));
		hcs.add(new Elem(0.015));
		hcs.add(new Elem(0.015));
		hcs.add(new Elem(0.01));

		
		a = hcs.size();
		r = 4;
		k = (int) (a / (r - 1) + 1);
	//	k = k == a ? k - 1 : k;
	}

	public void huffmancompress(List<Elem> hcs) {
		sort();
		for (int i = 1; i < k; ++i) {
		//	System.out.println(hcs.toString());
			combination(i);
			sort();
			System.out.println(hcs.toString());
		}
	}

	public void sort() {
		Comparator<Elem> comparator = new Comparator<Elem>() {
			
			@Override
			public int compare(Elem o1, Elem o2) {
				// TODO Auto-generated method stub
				BigDecimal data1 = new BigDecimal(o1.prob);
				BigDecimal data2 = new BigDecimal(o2.prob);
				if (data1.compareTo(data2) <= 0) {
					return 1;
				}
				return -1;
			}
		};
		Collections.sort(hcs, comparator);
	}

	public void combination(int ck) {
		int len = hcs.size();
		int i = 0, j = 0;
		double prob = 0;
		Elem newelem = new Elem(prob);
		newelem.flag = ck;
		if (ck == 1)
			i = (k - 1) * (r - 1);
		else
			i = len - r;
		j = i;
		while(j != len) {
			prob += hcs.get(i).prob;
			newelem.list.add(hcs.get(i));
			hcs.remove(i);
			j++;
		}
		newelem.prob = prob;
		hcs.add(newelem);
	}
	
	public void getHuffumanCompress() {
		HuffmanCompress hfs = new HuffmanCompress();
		hfs.huffmancompress(hcs);
		System.out.println("-----------------------");
	}
}