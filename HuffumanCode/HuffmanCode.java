package test;

import java.util.ArrayList;
import java.util.List;

public class HuffmanCode {
	public static List<Elem> codes;

	public HuffmanCode() {
		// TODO Auto-generated constructor stub
		codes = new ArrayList<Elem>();
	}

	public String toString() {
		String string = "";
		for (int i = 0; i < codes.size(); ++i) {
			System.out.println("code   " + codes.get(i).code);
			string =codes.get(i).prob + codes.get(i).code;
		}
		return string;
	}

	public static void main(String[] args) {
		HuffmanCompress hfs = new HuffmanCompress();
		hfs.getHuffumanCompress();
		new HuffmanCode();
		List<Elem> list = hfs.hcs;
		System.out.println(list.toString());

		// init codes
		for (int i = 0; i < list.size(); ++i) {
			String code = Integer.toString(i);
			list.get(i).code = code;
			codes.add(list.get(i));
		}

		for (int k = hfs.k - 1; k >= 1; --k) {
			Elem signElem = new Elem();
			for (int i = 0; i < codes.size(); ++i) {
				if (codes.get(i).flag == k) {
					signElem = codes.get(i);
					String signCode = signElem.code;
					System.out.println("signcode:" + signCode);
					List<Elem> signElemList = signElem.list;
					for (int j = 0; j < signElemList.size(); ++j) {
						String appendCode = Integer.toString(j);
						String signElemListCode = signCode +  appendCode;
						signElemList.get(j).code = signElemListCode;
						codes.add(signElemList.get(j));
					}
					break;
				}
			}
			codes.remove(signElem);
		}
		
		String string = "";
		for (int i = 0; i < codes.size(); ++i) {
			string =codes.get(i).prob +" " +  codes.get(i).code;
			System.out.println("code "+ string);
		}
	}
}
