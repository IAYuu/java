package calculator;

import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class Calculate implements ActionListener{
	DecimalFormat decimalFormat;
	static double storage;
	boolean flag;
	boolean judgepoint;
	boolean judechar;
	public static Stack<Object>input;
	static Stack<Object> output;
	Stack<Double> operand;

	public Calculate() {
		decimalFormat = new DecimalFormat("###################.###########");
		storage = 0;
		flag = false;
		judgepoint = false;
		judechar = false;
		input = new Stack<Object>();
		output = new Stack<>();
		operand = new Stack<>();
	}

	/**
	 * @param storage 存储区的值
	 * @param input 存放要计算的中缀表达式的栈
	 * @param operand 操作数栈
	 * @param cur current 触发事件发生的事件源的名称（即按钮名称）
	 * @param flag 判断input栈顶元素是否为操作符，栈顶若为操作符，则为true，反之为false
	 */
	public void actionPerformed(ActionEvent e) {
		String cur = e.getActionCommand();
		String preshow = CalUI.res.getText();
		/**
		 * @function 处理操作数键
		 * 1. 若上次的计算结果为出错或者输入栈的值为空
		 *    则preshow = "";
		 * 2. 此次输入的为操作数键
		 *  a.若当前情况为上次的计算结果为出错输入栈的值为空
		 *    则压入栈
		 *  b.若input栈顶元素为操作符
		 *    则压入栈（不可与a合并）
		 *  c.反之在input栈顶元素后面追加数字
		 */
		try {
			if (cur == ".") {
				input.set(input.size() - 1, decimalFormat.format(input.peek()) + cur);
				CalUI.res.setText(preshow + cur);
				judgepoint = true;
				return;
			}
			if (preshow.equals("=出错") || input.size() == 0)
				preshow = "";
			if(cur == "0" || cur == "1" || cur == "2" || cur == "3" 
					|| cur == "4"|| cur == "5" || cur == "6"
					|| cur == "7"|| cur == "8" || cur == "9") {
				if (judgepoint || judechar) {
					input.set(input.size() - 1, Double.parseDouble(input.peek() + cur));
					CalUI.res.setText(preshow + cur);
					if (judechar)
						judechar = false;
					else 
						judgepoint = false;
					return;
				}
				if (preshow.equals("=出错") || input.size() == 0) 
					input.push(Double.parseDouble(cur.toString()));
				else if (flag)
					input.push(Double.parseDouble(cur.toString()));

				else {
					String s = decimalFormat.format(Double.parseDouble(input.peek().toString()));
					input.set(input.size() - 1, Double.parseDouble(s + cur));
				}
				CalUI.res.setText(preshow + cur);
				flag = false;
				return;
			}

			/**
			 * @param preshow 按钮按下去之前的显示区
			 * @function 处理input栈以及显示区
			 *  a. 若在按下操作符键之前未输入操作数，则使用storage（上一次的计算结果）进行计算
			 *  b. 处理按下操作符键时，压入栈的操作符以及显示区的显示
			 */
			if (input.contains("%") && input.peek() instanceof Double) {
				input.set(input.size() - 2, "×");
				input.push("÷");
				input.push(100.0);
			}
			else if (input.contains("%") && input.peek() instanceof String) {
				input.set(input.size() - 1, "÷");
				input.push(100.0);
			}
			if (cur.equals("C")) {
				CalUI.res.setText("");
				input.clear();
			}
			if (cur.equals("CE")) {
				if (input.size() == 0)
					CalUI.res.setText("0");
				else {
					int len = 1;
					if (input.peek() instanceof String) {
						len = input.peek().toString().length();
						input.pop();
					}
					else {
						String s = decimalFormat.format(input.pop());
						if (s.length() != 1)
							input.push(Double.parseDouble(s.substring(0, s.length() - 1)));
					}
					if (input.size() == 0)
						CalUI.res.setText("");
					else
						CalUI.res.setText(preshow.substring(0, preshow.length() - len));
				}
			}

			if (input.size() == 0 && (cur == "<html>X<sup>2</sup></html>" || cur == "1/X" || cur.equals("%")
					||cur == "<html>X<sup>3</sup></html>" || cur == "<html>X<sup>y</sup></html>" || cur == "|X|"
					||cur == "x√y" || cur == "^" || cur == "×" || cur == "÷" || cur == "+" || cur == "-" || cur == "+/-")) {
				input.push(storage);
				CalUI.res.setText("" + storage);
				preshow = "" + storage;
			}

			if (cur.equals("+") || cur.equals("-") || cur.equals("×") || cur.equals("÷") || cur.equals("√")
					|| cur.equals("^") || cur.equals("(") || cur.equals(")") || cur.equals("%")) {
				if (cur.equals("%") && input.size() == 0)
					throw new CalculateException();
				if (input.size() != 0) {
					if (input.peek() instanceof String && input.peek() != ")" && (cur.equals("+") || cur.equals("-"))) {
						input.push(cur);
						CalUI.res.setText(preshow + cur);
						judechar = true;
						return;
					}
				}

				input.push(cur);
				CalUI.res.setText(preshow + cur);
			}
			else {
				if (cur.equals("+/-")) {
					if (input.peek() instanceof String) {
						input.push("-");
						CalUI.res.setText(preshow + "-");
						judechar = true;
					}
					else {
						String replace = decimalFormat.format((Double)input.peek());
						if (Double.parseDouble(input.peek().toString()) > 0)
							input.set(input.size() - 1, Double.parseDouble(decimalFormat.format(
									Double.parseDouble("-" + input.peek().toString()))));
						else
							input.set(input.size() - 1, Double.parseDouble(decimalFormat.format(
									Math.abs(Double.parseDouble(input.peek().toString())))));
						CalUI.res.setText(preshow.substring(0, preshow.lastIndexOf(replace)) 
								+ decimalFormat.format(input.peek()));
					}
					return;
				}

				if (cur.equals("X!")) {
					input.push("!");
					CalUI.res.setText(preshow + "!");
				}
				if (cur.equals("sin") || cur.equals("cos") || cur.equals("tan") || cur.equals("ln")) {
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push(cur);
					input.push("(");
					CalUI.res.setText(preshow + cur + "(");
				}
				if (cur.equals("<html>log<sub>10</sub></html>")) {
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push("log10");
					input.push("(");
					CalUI.res.setText(preshow + "log(");
				}
				if (cur.equals("<html>X<sup>2</sup></html>")) {
					input.push("^");
					input.push(2.0);
					CalUI.res.setText(preshow + "^2");
				}
				if (cur.equals("<html>X<sup>3</sup></html>")) {
					input.push("^");
					input.push(3.0);
					CalUI.res.setText(preshow + "^3");
				}
				if (cur.equals("<html>X<sup>y</sup></html>")) {
					input.push("^");
					CalUI.res.setText(preshow + "^");
				}
				if (cur.equals("<html>e<sup>x</sup></html>")) {
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push(Math.E);
					input.push("^");
					CalUI.res.setText(preshow + "e^");
				}
				if (cur.equals("x√y")) {
					input.push("^");
					input.push("(");
					input.push(1.0);
					input.push("÷");
					CalUI.res.setText(preshow + "^(1÷");
				}
				if (cur.equals("π")) {
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push(Math.PI);
					CalUI.res.setText(preshow + cur);
				}
				if (cur.equals("e")){
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push(Math.E);
					CalUI.res.setText(preshow + cur);
				}
				if (cur.equals("MR")) {
					if (input.size() != 0 && input.peek() instanceof Double)
						input.push("×");
					input.push(storage);
					CalUI.res.setText(preshow + cur);
				}		

				if (cur.equals("1/X"))
					storage = 1.0 / RPN();
				if (cur.equals("|X|"))
					storage = Math.abs(RPN());
				if (cur.equals("="))
					storage = RPN();
				if (cur.equals("1/X") || cur.equals("|X|") || cur.equals("=")) {
					CalUI.res.setText("=" + decimalFormat.format(storage));
					input.clear();
				}

			}
			flag = true;
		} catch(CalculateException e1) { 
			CalUI.res.setText("=出错");
			input.clear();
		}

	}


	/**
	 * 由后缀表达式求中缀表达式的值
	 * RPN, reverse Polish notation
	 */
	public double RPN() throws CalculateException{
		if (input.contains("(") && !input.contains(")"))
			input.push(")");
		ShuntingYard sy = new ShuntingYard();
		output = sy.shuntingyard(input);
		double curnum = 0;
		for(Object cur : output) {
			if (cur instanceof Double) {
				operand.push((Double)cur);
				continue;
			}
			else {
				switch (cur.toString()) {
				case "+":
					curnum = operand.pop() + operand.pop();	break;
				case "-":
					double top1 = operand.pop();
					curnum = operand.pop() - top1; break;
				case "×":
					curnum = operand.pop() * operand.pop(); break;
				case "÷":
					double top2 = operand.pop();
					curnum = operand.pop() / top2; break;
				case "√":
					curnum = Math.sqrt(operand.pop()); break;
				case "sin":
					curnum = Math.sin(operand.pop()); break;
				case "cos":
					curnum = Math.cos(operand.pop()); break;
				case "tan": 
					curnum = Math.tan(operand.pop()); break;
				case "log10":
					curnum = Math.log10(operand.pop()); break;
				case "ln":
					curnum = Math.log(operand.pop()); break;
				case "^":
					double top3 = operand.pop();
					curnum = Math.pow(operand.pop(), top3); break;
				case "!":
					if (operand.peek() == (new Double(operand.peek()).intValue()))
						curnum = fac(new Double(operand.pop()).intValue());
					else 
						throw new CalculateException(); break;
				default: throw new CalculateException();
				}
			}
			operand.push(curnum);
		}
		double res = operand.pop();
		return res;
	}

	public double fac(int n) {
		if (n == 1)
			return 1;
		else return fac(n - 1) * n;
	}
}
