package calculator;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class ShuntingYard {
	/**
	 * @param prioGraph 优先级表，按ops函数字符串的存放顺序进行比较
	 * @param ops 为了得到函数字符串在优先级表里的位置，并按该顺序构造优先级表
	 */
	Stack<Object> output;
	Stack<String> operator;
	List<String> ops;
	static String[][] prioGraph;

	public ShuntingYard() {
		output = new Stack<>();
		operator = new Stack<>();
		ops = Arrays.asList("+","-","×","÷","√","sin","cos","tan",
				"log10","ln","^","!","(",")","#");
		prioGraph = new String[15][15];
	}

	public void makePrioGraph() {
		for (int j = 0; j < 15; j++) {
			for (int i = 0; i < 15; i++) {
				switch (j) {
				case 0:
				case 1:
					if (i != 12 && i != 14)
						prioGraph[i][j] = ">";
					else 
						prioGraph[i][j] = "<"; break;
				case 2:
				case 3: 
					if (i != 0 && i != 1 && i != 12 && i != 14)
						prioGraph[i][j] = ">";
					else
						prioGraph[i][j] = "<"; break;
				case 4:
				case 5:	
				case 6:
				case 7: 
				case 8:
				case 9:
					if (i != 0 && i != 1 && i != 12 
					&& i != 14 && i != 2 && i != 3)
						prioGraph[i][j] = ">";
					else 
						prioGraph[i][j] = "<"; break;
				case 10:
					if (i == 11 || i == 13)
						prioGraph[i][j] = ">";
					else 
						prioGraph[i][j] = "<"; break;
				case 11:
					if (i == 11 || i == 13)
						prioGraph[i][j] = ">";
					else 
						prioGraph[i][j] = "<"; break;
				case 12:
					if (i == 13)
						prioGraph[i][j] = "erro";
					else
						prioGraph[i][j] = "<"; break;
				case 13:
					if (i == 12)
						prioGraph[i][j] = "=";
					else if (i == 14)
						prioGraph[i][j] = "erro";
					else
						prioGraph[i][j] = ">"; break;
				case 14:
					if (i == 12)
						prioGraph[i][j] = "erro";
					else if (i == 14)
						prioGraph[i][j] = "=";
					else 
						prioGraph[i][j] = ">";
				default: 
					break;
				}
			}
		}
	}

	public  Stack<Object> shuntingyard(Stack<Object> input) {
		makePrioGraph();
		try {
			operator.push("#");
			for (Object cur : input) {
				if (cur instanceof Double)
					output.push(cur);
				else if (cur instanceof String) {
					String priority = Prior(cur,operator.peek());
					if (priority == "<")
						operator.push(cur.toString());
					else if (priority == ">") {
						while(Prior(cur, operator.peek()) == ">") 
							output.push(operator.pop());
						if (Prior(cur, operator.peek()) == "=") {
							operator.pop();
							continue;
						}
						operator.push(cur.toString());
					}
					else if (Prior(cur, operator.peek()) == "=")
						operator.pop();
					else if (priority == "erro")
						throw new CalculateException();
				}
			}
		}catch (CalculateException e){
			CalUI.res.setText("=出错");
		}
		/**
		 * 将operator结尾的字符串都放入output
		 */
		while(operator.peek() != "#")
			output.push(operator.pop());
		return output;
	}

	public String Prior(Object cur, String top) {
		return prioGraph[ops.indexOf(top)][ops.indexOf(cur)];
	}

}
