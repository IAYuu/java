package calculator;

import java.awt.*;
import javax.swing.*;
public class CalUI extends JFrame{
	static JLabel res;
	
	public CalUI(){
		JButton[] buttons = new JButton[39];
		buttons[0] = new JButton("sin");
		buttons[1] = new JButton("cos");
		buttons[2] = new JButton("tan");
		buttons[3] = new JButton("<html>log<sub>10</sub></html>");
		buttons[4] = new JButton("C");
		buttons[5] = new JButton("+/-");
		buttons[6] = new JButton("CE");
		buttons[7] = new JButton("��");
		buttons[8] = new JButton("1/X");
		buttons[9] = new JButton("<html>X<sup>2</sup></html>");
		buttons[10] = new JButton("<html>X<sup>3</sup></html>");
		buttons[11] = new JButton("<html>X<sup>y</sup></html>");
		buttons[12] = new JButton("7");
		buttons[13] = new JButton("8");
		buttons[14] = new JButton("9");
		buttons[15] = new JButton("��");
		buttons[16] = new JButton("|X|");
		buttons[17] = new JButton("X!");
		buttons[18] = new JButton("��");
		buttons[19] = new JButton("x��y");
		buttons[20] = new JButton("4");
		buttons[21] = new JButton("5");
		buttons[22] = new JButton("6");
		buttons[23] = new JButton("-");
		buttons[24] = new JButton("��");
		buttons[25] = new JButton("e");
		buttons[26] = new JButton("<html>e<sup>x</sup></html>");
		buttons[27] = new JButton("ln");
		buttons[28] = new JButton("1");
		buttons[29] = new JButton("2");
		buttons[30] = new JButton("3");
		buttons[31] = new JButton("+");
		buttons[32] = new JButton("(");
		buttons[33] = new JButton(")");
		buttons[34] = new JButton("%");
		buttons[35] = new JButton("MR");
		buttons[36] = new JButton("0");
		buttons[37] = new JButton(".");
		buttons[38] = new JButton("=");
		res = new JLabel("0");
		res.setFont(new Font("",Font.PLAIN,30));
	
		
		Calculate monitor = new Calculate();
		Dimension dimension = new Dimension(100, 50);
		JPanel panel = new JPanel();
		for (JButton button : buttons) {
			button.setFocusPainted(false);
			button.addActionListener(monitor);
			button.setPreferredSize(dimension);
			panel.add(button);
			button.setBackground(Color.black);
			button.setForeground(Color.white);
			button.setFont(new Font("",Font.PLAIN,17));
		}
		buttons[7].setForeground(Color.red);
		buttons[15].setForeground(Color.red);
		buttons[23].setForeground(Color.red);
		buttons[31].setForeground(Color.red);
		buttons[38].setForeground(Color.red);
		buttons[36].setPreferredSize(new Dimension(205,50));
	
		panel.setBackground(Color.black);
	
		JFrame frame = new JFrame();
		JPanel cont = (JPanel)frame.getContentPane();
		
		cont.add(res);
		cont.add(panel);
		cont.setBackground(Color.gray);
		
		frame.setSize(916,439);
		frame.setLocation(400,400);
		frame.setLayout(null);
		res.setBounds(32,0,900,100);
		panel.setBounds(0,100,900,300);
		
		frame.setVisible(true);
	}
}