
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Year;

public class TestChangeButtonColor extends Frame implements ActionListener {
	Button[] buttons;

	public TestChangeButtonColor() {
		buttons = new Button[9];
		for (int i = 0; i < 9; ++i) {
			buttons[i] = new Button("" + (i + 1));
			add(buttons[i]);
			buttons[i].addActionListener(this);
		}
		setLayout(new FlowLayout());
		setSize(500, 500);
		setLocation(500, 500);
		setBackground(new Color(149, 194, 206));
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		int r = (int)(Math.random() * 256);
		int g = (int)(Math.random() * 256);
		int b = (int)(Math.random() * 256);
		Button button = (Button)e.getSource();
		button.setBackground(new Color(r, g, b));
	}

	public static void main(String[] args) {
		new TestChangeButtonColor();
	}
}
